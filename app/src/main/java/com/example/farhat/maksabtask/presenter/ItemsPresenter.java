package com.example.farhat.maksabtask.presenter;

import android.util.Log;
import android.widget.Toast;

import com.example.farhat.maksabtask.model.Data;
import com.example.farhat.maksabtask.model.Item;
import com.example.farhat.maksabtask.network.GetDataService;
import com.example.farhat.maksabtask.network.RetrofitClientInstance;
import com.example.farhat.maksabtask.view.activities.MainActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemsPresenter {

    private MainActivity view;

    public ItemsPresenter(MainActivity view) {
        this.view = view;
    }

    public void getItems() {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<Data> call = service.getItems();
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                view.hideProgressDialog();
                if (response.isSuccessful()) {
                    Log.e("Response :", "succeed and Response is " + response.body().getResult().getData());
                    view.updateUi(response.body().getResult().getData());
                    Log.e("Message :", "Message  " + response.message());
                } else {
                    Toast.makeText(view, "Request Failed", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                view.hideProgressDialog();
                Log.e("Response :", "failed");
                //Toast.makeText(view, "Request Failed Check The Response", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public interface View {
        void showProgressDialog();
        void hideProgressDialog();
        void updateUi(List<Item> data);
    }
}



