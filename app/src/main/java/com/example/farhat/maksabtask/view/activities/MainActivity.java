package com.example.farhat.maksabtask.view.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.farhat.maksabtask.R;
import com.example.farhat.maksabtask.model.Item;
import com.example.farhat.maksabtask.presenter.ItemsPresenter;
import com.example.farhat.maksabtask.view.adapters.ItemsAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemsPresenter.View {

    public RecyclerView mItemsRecyclerView;
    public LinearLayoutManager mLayoutManager;
    public ItemsAdapter mItemsAdapter;
    public ProgressBar mloadingBar;
    private static ItemsPresenter mItemsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mItemsRecyclerView = (RecyclerView) findViewById(R.id.items_recycler_view);
        mItemsRecyclerView.setHasFixedSize(true);
        mloadingBar = findViewById(R.id.loading_data_progress_bar);
        showProgressDialog();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isNetworkAvailable(this)) {
            hideProgressDialog();
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        if (mItemsPresenter == null) {
            mItemsPresenter = new ItemsPresenter(this);
        }

        mItemsPresenter.getItems();
    }

    @Override
    public void showProgressDialog() {
        mloadingBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressDialog() {
        mloadingBar.setVisibility(View.GONE);
    }

    @Override
    public void updateUi(List<Item> data) {
        mLayoutManager = new LinearLayoutManager(this);
        mItemsRecyclerView.setLayoutManager(mLayoutManager);
        mItemsAdapter = new ItemsAdapter(data);
        mItemsRecyclerView.setAdapter(mItemsAdapter);
    }

    private boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
