package com.example.farhat.maksabtask.network;

import com.example.farhat.maksabtask.model.Data;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("5bb39db63300002a00cad329")
    Call<Data> getItems();
}
