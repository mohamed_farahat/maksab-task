package com.example.farhat.maksabtask.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.farhat.maksabtask.R;
import com.example.farhat.maksabtask.model.Data;
import com.example.farhat.maksabtask.model.Item;
import com.example.farhat.maksabtask.network.GetDataService;
import com.example.farhat.maksabtask.network.RetrofitClientInstance;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static int LOADING = 1;
    private static int DATA = 2;

    private List<Item> itemList;
    private int pages = 5;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mBrand, mTitle;
        public ImageView mItemImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            mBrand = itemView.findViewById(R.id.brand_text_view);
            mTitle = itemView.findViewById(R.id.title_text_view);
            mItemImage = itemView.findViewById(R.id.background_image_view);
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar mLoadingBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            mLoadingBar = itemView.findViewById(R.id.progress_loadingBar);
        }
    }

    public ItemsAdapter(List<Item> myDataset) {
        itemList = myDataset;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LOADING) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            LoadingViewHolder vh = new LoadingViewHolder(v);
            return vh;
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_row, parent, false);
            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (position == getItemCount() - 1 && pages == 0) {
            LoadingViewHolder holder = (LoadingViewHolder) viewHolder;
            holder.mLoadingBar.setVisibility(View.GONE);
        }
        if (position == getItemCount() - 1 && pages > 0) {
            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<Data> call = service.getItems();
            call.enqueue(new Callback<Data>() {
                @Override
                public void onResponse(Call<Data> call, Response<Data> response) {

                    if (response.isSuccessful()) {
                        Log.e("Response :", "succeed and Response is " + response.body().getResult().getData());
                        itemList.addAll(response.body().getResult().getData());
                        pages--;
                        notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<Data> call, Throwable t) {
                    Log.e("Response :", "failed");
                }
            });
        } else if (position < getItemCount() - 1) {
            MyViewHolder holder = (MyViewHolder) viewHolder;
            Item item = itemList.get(position);
            holder.mBrand.setText(item.getBrand());
            holder.mTitle.setText(item.getDesc());
            Log.e("ImageURL", item.getImage());
            Picasso.get().load(item.getImage()).into(holder.mItemImage);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == getItemCount() - 1 ? LOADING : DATA;
    }

    @Override
    public int getItemCount() {
        return itemList.size() + 1;
    }
}